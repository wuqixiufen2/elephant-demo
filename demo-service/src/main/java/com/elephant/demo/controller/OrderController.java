package com.elephant.demo.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.elephant.demo.model.param.OrderCreateParam;
import com.elephant.demo.model.param.OrderPageParam;
import com.elephant.demo.model.vo.OrderPageVo;
import com.elephant.demo.model.vo.OrderVo;
import com.elephant.demo.service.IOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author xiufen.huang
 * @description:
 * @date 2023-01-19-10:49
 */
@Api(value = "OrderController", tags = "测试Controller")
@Slf4j
@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final IOrderService orderService;

    @ApiOperation(value = "获取订单信息")
    @GetMapping(value = "/info/{orderId}")
    public OrderVo getByOrderId(@PathVariable("orderId") String orderId){
        OrderVo orderVo = orderService.getById(orderId);
        return orderVo;
    }

    @ApiOperation("订单分页列表")
    @PostMapping("/page")
    public IPage<OrderPageVo> getOrderPage(@RequestBody OrderPageParam param){
        return orderService.getOrderPage(param);
    }

    @ApiOperation("订单创建")
    @PostMapping("/create")
    public Boolean createOrder(@Valid @RequestBody OrderCreateParam param){
        return orderService.createOrder(param);
    }

}
