package com.elephant.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author xiufen.huang
 * @description:
 * @date 2023-01-08-18:47
 */
@Api(value = "TestController", tags = "测试Controller")
@Slf4j
@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {

    @ApiOperation(value = "测试接口")
    @GetMapping("/index")
    public String test() {
        return "ok";
    }

}
