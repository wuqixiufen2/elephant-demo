package com.elephant.demo.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elephant.demo.model.entity.Order;
import com.elephant.demo.model.param.OrderPageParam;
import com.elephant.demo.model.vo.OrderPageVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 外贸订单信息-Mapper
 * @author xiufen.huang
 * @date 2022-12-12 16:13
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    IPage<OrderPageVo> getOrderPage(Page page, @Param("param") OrderPageParam param);

}