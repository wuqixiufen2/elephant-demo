package com.elephant.demo.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.elephant.demo.model.entity.Order;
import com.elephant.demo.model.param.OrderCreateParam;
import com.elephant.demo.model.param.OrderPageParam;
import com.elephant.demo.model.vo.OrderPageVo;


/**
 * @author xiufen.huang
 * @description:
 * @date 2022-12-21-15:38
 */
public interface IOrderRepository extends IService<Order> {

    /**
     * 获取订单信息
     * @param orderId 订单id
     * @return com.elephant.demo.model.entity.Order
     * @author xiufen.huang
     * @date 2023-01-19 11:01
     */
    Order getById(String orderId);

    /**
     * 订单分页列表
     * @param param 查询参数
     * @return com.baomidou.mybatisplus.core.metadata.IPage<com.elephant.demo.model.vo.OrderPageVo>
     * @author xiufen.huang
     * @date 2023-01-19 11:01
     */
    IPage<OrderPageVo> getOrderPage(OrderPageParam param);

    /**
     * 保存订单数据
     * @param param
     * @return java.lang.Boolean
     * @author xiufen.huang
     * @date 2022-12-16 15:50
     */
    Boolean saveOrder(OrderCreateParam param);
}
