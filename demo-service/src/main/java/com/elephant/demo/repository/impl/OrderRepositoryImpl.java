package com.elephant.demo.repository.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.elephant.demo.model.entity.Order;
import com.elephant.demo.model.param.OrderCreateParam;
import com.elephant.demo.model.param.OrderPageParam;
import com.elephant.demo.model.vo.OrderPageVo;
import com.elephant.demo.repository.IOrderRepository;
import com.elephant.demo.repository.mapper.OrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author xiufen.huang
 * @description:
 * @date 2022-12-21-15:39
 */
@Slf4j
@Service
public class OrderRepositoryImpl extends ServiceImpl<OrderMapper, Order>  implements IOrderRepository {

    @Override
    public Order getById(String orderId) {
        Order order = this.baseMapper.selectById(orderId);
        return order;
    }

    @Override
    public IPage<OrderPageVo> getOrderPage(OrderPageParam param) {
        Page page = new Page(param.getPage().getCurrent(), param.getPage().getSize());
        return baseMapper.getOrderPage(page, param);
    }

    @Override
    public Boolean saveOrder(OrderCreateParam param) {
        // 参数赋值
        Order order = new Order();
        order.setOrderNo(param.getOrderNo());
        order.setCustomerName(param.getCustomerName());
        order.setCustomerEmail(param.getCustomerEmail());
        order.setCreateTime(new Date());
        order.setUpdateTime(new Date());
        // 订单创建时，默认是0-正常
        order.setStatus(0);
        // 持久化数据
        return this.save(order);
    }

}
