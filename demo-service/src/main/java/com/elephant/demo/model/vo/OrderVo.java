package com.elephant.demo.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xiufen.huang
 * @description:
 * @date 2022-12-25-12:36
 */
@Data
public class OrderVo implements Serializable {

    @ApiModelProperty(value = "主键")
    private String id;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "订单状态: 0-正常, 1-已删除")
    private Integer status;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "客户邮箱")
    private String customerEmail;

    @ApiModelProperty(value = "货品状态: 1-备车中,2-出口手续办理中,3-转移待出口（手续办理完成）,4-报关完成,5-车辆达到指定港口,6-运输中,7-已抵达,8-确认收货")
    private Integer productStatus;

}
