package com.elephant.demo.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.elephant.demo.model.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 外贸订单信息-Entity
 *
 * @author xiufen.huang
 * @date 2022-12-12 16:13
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@TableName("ft_order")
public class Order extends BaseEntity {

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 客户邮箱
     */
    private String customerEmail;

    /**
     * 货品状态: 1-备车中,2-出口手续办理中,3-转移待出口（手续办理完成）,4-报关完成,5-车辆达到指定港口,6-运输中,7-已抵达,8-确认收货
     */
    private Integer productStatus;

    /**
     * 备注
     */
    private String remark;

}