package com.elephant.demo.model.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class OrderPageParam implements Serializable {

    @ApiModelProperty(value = "分页参数", required = true)
    private Query page;

    @ApiModelProperty(value = "搜索关键词:订单号/客户名称/客户邮箱")
    private String searchWord;

}
