package com.elephant.demo.model.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Description: 订单-创建信息-参数
 * @author: xiufen.huang
 * @date: 2022-12-12 17:27
 */
@Data
public class OrderCreateParam implements Serializable {

    @ApiModelProperty(value = "订单编号", required = true)
    @NotBlank(message = "订单编号不可以为空")
    private String orderNo;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "客户邮箱")
    @Email(message = "邮箱格式不正确")
    private String customerEmail;

}