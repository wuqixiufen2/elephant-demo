package com.elephant.demo.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xiufen.huang
 * @description:
 * @date 2023-01-08-21:09
 */
@ApiModel(description = "查询条件")
@Data
public class Query {
    @ApiModelProperty("当前页")
    private Integer current = 1;
    @ApiModelProperty("每页的数量")
    private Integer size = 10;
}
