package com.elephant.demo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.elephant.demo.model.param.OrderCreateParam;
import com.elephant.demo.model.param.OrderPageParam;
import com.elephant.demo.model.vo.OrderPageVo;
import com.elephant.demo.model.vo.OrderVo;

/**
 * @author xiufen.huang
 * @description:
 * @date 2022-12-25-16:00
 */
public interface IOrderService {

    OrderVo getById(String orderId);

    /**
     * 订单分页列表
     * @param param
     * @return
     */
    IPage<OrderPageVo> getOrderPage(OrderPageParam param);

    /**
     * 订单创建
     * @param param 参数
     * @return java.lang.Boolean
     * @author xiufen.huang
     * @date 2022-12-14 10:21
     */
    Boolean createOrder(OrderCreateParam param);

}
