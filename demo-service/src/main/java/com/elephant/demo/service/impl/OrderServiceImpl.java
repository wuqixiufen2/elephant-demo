package com.elephant.demo.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.elephant.demo.model.entity.Order;
import com.elephant.demo.model.param.OrderCreateParam;
import com.elephant.demo.model.param.OrderPageParam;
import com.elephant.demo.model.vo.OrderPageVo;
import com.elephant.demo.model.vo.OrderVo;
import com.elephant.demo.repository.IOrderRepository;
import com.elephant.demo.service.IOrderService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
/**
 * @author xiufen.huang
 * @description:
 * @date 2022-12-25-16:00
 */
@Slf4j
@Service
@AllArgsConstructor
public class OrderServiceImpl implements IOrderService {

    private final IOrderRepository orderRepository;

    @Override
    public OrderVo getById(String orderId) {
        log.info("请求参数：{}", orderId);
        Order order = orderRepository.getById(orderId);
        if (order == null){
            return null;
        }
        OrderVo orderVo = new OrderVo();
        orderVo.setId(order.getId());
        orderVo.setCreateTime(order.getCreateTime());
        orderVo.setUpdateTime(order.getUpdateTime());
        orderVo.setStatus(order.getStatus());
        orderVo.setOrderNo(order.getOrderNo());
        orderVo.setCustomerName(order.getCustomerName());
        orderVo.setCustomerEmail(order.getCustomerEmail());
        return orderVo;
    }

    @Override
    public IPage<OrderPageVo> getOrderPage(OrderPageParam param) {
        log.info("订单分页列表，请求参数：{}", param);
        IPage<OrderPageVo> listPage = orderRepository.getOrderPage(param);
        return listPage;
    }

    @Override
    public Boolean createOrder(OrderCreateParam param) {
        log.info("订单创建，请求参数：{}",param);
        // 持久化数据
        Boolean result = orderRepository.saveOrder(param);
        return result;
    }
}
