CREATE TABLE `ft_order` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int DEFAULT NULL COMMENT '业务状态: 0-正常, 1-已删除',
  `is_deleted` bit(1) DEFAULT b'0' COMMENT '是否删除',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `customer_name` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `customer_email` varchar(100) DEFAULT NULL COMMENT '客户邮箱',
  `product_status` int DEFAULT NULL COMMENT '货品状态: 1-备车中,2-出口手续办理中,3-转移待出口（手续办理完成）,4-报关完成,5-车辆达到指定港口,6-运输中,7-已抵达,8-确认收货',
  `remark` varchar(256) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `idx_customer_name` (`customer_name`) USING BTREE,
  KEY `idx_order_no_email_name` (`order_no`,`customer_email`,`customer_name`) USING BTREE COMMENT 'PC后台查询组合索引',
  KEY `idx_customer_email_name` (`customer_email`,`customer_name`) USING BTREE
)COMMENT='外贸订单信息';